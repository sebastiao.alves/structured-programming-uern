# Programação estruturada - Ciência da Computação - UERN \[[English version]\](#english-version)

Exemplos de código fonte C++ e slides da disciplina Programação Estruturada do Curso de Ciência da Computação da UERN-Mossoró.

São bem vindas contribuições que:
* Adicionem comentários aos programas;
* Traduzam variáveis e comentários para Inglês / Português;
* Resolvam os problemas propostos com outras abordagens.

#english-version
# Strutured Programming - Computer Science - UERN

Slides and C++ example source code for Structured Programming discipline in Computer Science  course at UERN-Mossoró-Brazil.

We welcome contributions that:
* Add comments to programs;
* Translate variables and comments to English / Portuguese;
* Solve proposed problems with another approaches.

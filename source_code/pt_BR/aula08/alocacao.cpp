#include <iostream>
#include <new>

using namespace std;
int main(int argc, char **argv)
{
	int ***mat;
	// Aloca 3 matrizes
	mat = new int ** [3];
	
	// Alocar o número de linhas de cada matriz
	for(int i=0;i<3;i++){
		mat[i] = new int *[3];
		// Alocar 10 colunas em cada linha da matriz
		for(int j=0;j<3;j++)
			mat[i][j] = new int [10];
	}
	
	for (int i=0;i<3;i++){
		for(int j=0;j<3;j++)
			delete []mat[i][j];
		delete []mat[i];
	}
	delete []mat;
	
	
	
	double **mat2 = new double * [4];
	for(int i=0;i<4;i++)
		mat2[i] = new double [6];
		
	for(int i=0;i<4;i++)
		delete [] mat2[i];
		
	delete []mat2;
		
	
	
	
	
	
	return 0;
}


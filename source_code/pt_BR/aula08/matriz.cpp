#include <iostream>

using namespace std;

int **alocaMatriz (size_t numLinhas, size_t numColunas){
    int **matriz;
    matriz = new int*[numLinhas];
    for (size_t i=0;i<numLinhas;i++)
        matriz[i] = new int[numColunas];
    return matriz;
}

void desalocaMatriz(int **matriz, size_t numLinhas){
    for(size_t i=0;i<numLinhas;i++)
        delete [] matriz[i];
    delete []matriz;
}

void leDadosMatriz(int **matriz, size_t numLinhas, size_t numColunas){
    for(size_t i=0;i<numLinhas;i++)
        for(size_t j=0;j<numColunas;j++){
            cout << "Digite o elemento [" << i << "][" << j << "] da matriz: ";
            cin >> matriz[i][j];
        }
}

//void exibeMatriz(int matriz[3][3], size_t numLinhas, size_t numColunas){
void exibeMatriz(int **matriz, size_t numLinhas, size_t numColunas){
    cout << "[";
    for(size_t i=0;i<numLinhas;i++){
        cout << "[";
        for (size_t j=0;j<numColunas;j++){
            cout << matriz[i][j] << ",";
        }
        cout << "]," << endl;
    }
    cout << "]" << endl;
}

char exibeMenu(){
    char opcao;
    do{
        cout << "Escolha uma opção:\n1 - Transpor matriz\n2 - Trocar linhas\n3 - Trocar colunas\n4 - Sair\nDigite: ";
        cin >> opcao;
    }while(opcao<'1' || opcao>'4');
    return opcao;
}

void trocaLinhas(int **matriz, size_t linha1, size_t linha2){
    int *aux=matriz[linha1];
    matriz[linha1]=matriz[linha2];
    matriz[linha2]=aux;
}

void trocaColunas(int **matriz, size_t numLinhas, size_t coluna1, size_t coluna2){
    for(size_t i=0;i<numLinhas;i++){
        int aux = matriz[i][coluna1];
        matriz[i][coluna1] = matriz[i][coluna2];
        matriz[i][coluna2] = aux;
    }
}

int ** transpoeMatriz (int **matriz, size_t &numLinhas, size_t &numColunas){
    int **nova = alocaMatriz(numColunas, numLinhas);
    for(size_t i=0;i<numLinhas;i++)
        for(size_t j=0;j<numColunas;j++){
            nova[j][i] = matriz[i][j];
        }
    desalocaMatriz(matriz, numLinhas);
    int aux=numLinhas;
    numLinhas=numColunas;
    numColunas=aux;
    return nova;
}

int main(){
    size_t numLinhas, numColunas;
    cout << "Digite o número de linhas e colunas da matriz: ";
    cin >> numLinhas >> numColunas;
    int **matriz = alocaMatriz(numLinhas, numColunas);
    leDadosMatriz(matriz, numLinhas, numColunas);
    char opcao;
    size_t l1, l2, c1, c2;
    do{
        opcao = exibeMenu();
        switch(opcao){
            case '1':
                matriz = transpoeMatriz(matriz, numLinhas, numColunas);
                exibeMatriz(matriz, numLinhas, numColunas);
                break;
            case '2':
                cout << "Digite os números das linhas a serem trocadas: ";
                cin >> l1 >> l2;
                trocaLinhas(matriz, l1, l2);
                exibeMatriz(matriz, numLinhas, numColunas);
                break;
            case '3':
                cout << "Digite os números das colunas a serem trocadas: ";
                cin >> c1 >> c2;
                trocaColunas(matriz, numLinhas, c1, c2);
                exibeMatriz(matriz, numLinhas, numColunas);
                break;
            case '4':
                cout << "Matriz final: " << endl;
                exibeMatriz(matriz, numLinhas, numColunas);
        }
    }while(opcao!='4');
    desalocaMatriz(matriz, numLinhas);
    return 0;
}

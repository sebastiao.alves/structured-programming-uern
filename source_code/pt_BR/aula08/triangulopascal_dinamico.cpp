#include <iostream>
using namespace std;
int main(){
	int n;
	cout << "Digite o número de linhas de um triângulo de pascal: ";
	cin >> n;
	int **trianguloPascal;
	trianguloPascal=new int *[n];
	for(int i=0;i<n;i++){
		trianguloPascal[i]=new int[i+1];
		trianguloPascal[i][0]=1;
		trianguloPascal[i][i]=1;
		for(int j=1;j<i;j++)
			trianguloPascal[i][j]=trianguloPascal[i-1][j-1]+trianguloPascal[i-1][j];
	}
	cout << "O triangulo de Pascal para " << n << " linhas: " << endl;
	for(int i=0;i<n;i++){
		for(int j=0;j<=i;j++)
			cout << trianguloPascal[i][j] << " ";
		cout << endl;
	}
	
	
	cout << "(x+y)^0=1" << endl;
	
	for(int i=1;i<n;i++){
		cout << "(x+y)^" << i << "=";
		for(int j=0;j<=i;j++){
			if(trianguloPascal[i][j]!=1)
				cout << trianguloPascal[i][j];
			if(i-j!=0){
				if(i-j==1)
					cout << "x";
				else
					cout << "x^"<< i-j;
			}
			if(j!=0){
				if(j==1)
					cout << "y";
				else
					cout << "y^" << j;
			}
			if(j!=i)
				cout << "+";
		}
		cout << endl;
	}
	for(int i=0;i<n;i++)
		delete []trianguloPascal[i];
	delete [] trianguloPascal;
	return 0;
}

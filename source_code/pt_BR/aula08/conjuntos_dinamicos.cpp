#include <iostream>
#include <algorithm>

using namespace std;

int main(){
	
	size_t tamConjunto1;
	cout << "Digite o tamanho do primeiro conjunto: ";
	cin >> tamConjunto1;
	
	// Alocando e lendo o conjunto 1
	int * conjunto1;
	conjunto1 = new int[tamConjunto1];
	for(size_t i=0;i<tamConjunto1;i++){
		cout << "Digite o " << (i+1) << "º elemento do 1º conjunto: ";
		cin >> conjunto1[i];
	}
	sort(conjunto1, conjunto1+tamConjunto1);
	cout << "Conjunto 1 ordenado" << endl;
	for(size_t i=0;i<tamConjunto1;i++)
		cout << conjunto1[i] << " ";
	cout << endl;
	
	
	size_t tamConjunto2;
	cout << "Digite o tamanho do segundo conjunto: ";
	cin >> tamConjunto2;
	
	// Alocando e lendo o conjunto 2
	int * conjunto2;
	conjunto2 = new int[tamConjunto2];
	for(size_t i=0;i<tamConjunto2;i++){
		cout << "Digite o " << (i+1) << "º elemento do 2º conjunto: ";
		cin >> conjunto2[i];
	}
	sort(conjunto2, conjunto2+tamConjunto2);
	cout << "Conjunto 2 ordenado" << endl;
	for(size_t i=0;i<tamConjunto2;i++)
		cout << conjunto2[i] << " ";
	cout << endl;
	
	size_t tamUniao=tamConjunto1+tamConjunto2;
	int * uniao = new int[tamUniao];
	set_union(conjunto1, conjunto1+tamConjunto1, conjunto2, conjunto2+tamConjunto2, uniao);
	int numZeros = count(uniao, uniao+tamConjunto1+tamConjunto2, 0);
	
	tamUniao -= numZeros;
	int *uniaoFinal = new int [tamUniao];
	copy(uniao, uniao+tamUniao, uniaoFinal);
	cout << "União dos conjuntos: " << endl;
	for(size_t i=0;i<tamUniao;i++)
		cout << uniaoFinal[i] << " ";
	cout << endl;
	
	size_t tamIntersecao = tamConjunto1<tamConjunto2?tamConjunto1:tamConjunto2;
	int * intersecao = new int[tamIntersecao];
	set_intersection(conjunto1, conjunto1+tamConjunto1, conjunto2, conjunto2+tamConjunto2, intersecao);
	
	tamIntersecao = numZeros;
	int *intersecaoFinal = new int [tamIntersecao];
	copy(intersecao, intersecao+tamIntersecao, intersecaoFinal);
	cout << "Interseção dos conjuntos: " << endl;
	for(size_t i=0;i<tamIntersecao;i++)
		cout << intersecaoFinal[i] << " ";
	cout << endl;
	
	//Desalocando as variáveis
	delete []conjunto1;
	delete []conjunto2;
	delete []uniao;
	delete []uniaoFinal;
	delete []intersecao;
	delete []intersecaoFinal;
	return 0;
}

#include <iostream>
using namespace std;
int main(){
	int numero, divisor=2;
	bool mesmoDivisor=false;
	cout << "Digite um número para saber se ele é formado por multiplicação de números primos distintos: ";
	cin >> numero;
	do{
		if(numero%divisor==0){
			if(mesmoDivisor)
				break;
			else
				mesmoDivisor=true;
			numero /= divisor;
		}else{
			++divisor;
			mesmoDivisor=false;
		}
	}while(numero>1);
	if(numero>1 && mesmoDivisor)
		cout << "Não. O divisor " << divisor << " é repetido. " << endl;
	else
		cout << "Sim. O número é formado por fatores primos distintos. " << endl;
	return 0;
}

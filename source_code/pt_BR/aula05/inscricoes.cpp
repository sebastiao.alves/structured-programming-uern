#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	const double INSC_ALUNO=30.0, INSC_PROF=70.0, INSC_MERC=100.0, MINICURSO=20.0;
	const double TAXA_DEBITO=0.0199, TAXA_CREDITO=0.0498;
	const int ALUNO=1, PROFESSOR=2, PROFISSIONAL=3, BOLETO=1, DEBITO=2, CREDITO=3;
	int tipoInscricao, formaPagamento;
	char minicurso;
	double valorTotal;
	do{
		cout << "Digite o tipo de inscrição:\n1 - ALUNO\n2 - PROFESSOR\n3 - PROFISSIONAL DO MERCADO\n";
		cin >> tipoInscricao;
		switch(tipoInscricao){
			case ALUNO:
				valorTotal=INSC_ALUNO;
				break;
			case PROFESSOR:
				valorTotal=INSC_PROF;
				break;
			case PROFISSIONAL:
				valorTotal=INSC_MERC;
				break;
			default:
				cout << "Opção inválida. Tente novamente." << endl;
		}
	}while (tipoInscricao!=ALUNO && tipoInscricao!=PROFESSOR && tipoInscricao!=PROFISSIONAL);
	
	do{
		cout << "Incluir minicurso? Digite S (sim) ou N (não)" << endl;
		cin>> minicurso;
		switch(minicurso){
			case 's':
			case 'S':
				valorTotal+=MINICURSO;
				break;
			case 'n':
			case 'N':
				break;
			default:
				cout << "Opção inválida. Tente novamente" << endl;
		}
	}while(minicurso!='s' && minicurso!='S' && minicurso !='n' && minicurso!='N');
	do{
		cout << "Qual a forma de pagamento:\n1 - BOLETO\n2 - CARTÃO DE DÉBITO\n3 - CARTÃO DE CRÉDITO\n";
		cin >> formaPagamento;
		switch(formaPagamento){
			case BOLETO:
				break;
			case DEBITO:
				valorTotal*=1+TAXA_DEBITO; // valorTotal = valorTotal + TAXA_DEBITO*valorTotal
				break;
			case CREDITO:
				valorTotal*=1+TAXA_CREDITO;
			default:
				cout << "Opção inválida.Tente novamente." << endl;
		}
	}while(formaPagamento!=BOLETO && formaPagamento!=DEBITO && formaPagamento!=CREDITO);
	cout << fixed << setprecision(2);
	cout << "O valor total a pagar é: " << valorTotal << endl;
	return 0;
}

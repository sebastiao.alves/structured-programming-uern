#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	for (int tabuada=1; tabuada<=9 ; tabuada+=3){
		cout << "Tabuada do número " << tabuada << endl;
		for (int n1=1 ; n1<=10; n1++){
			for(int j=tabuada;j<tabuada+3;j++)
				cout << setw(10) << j << " x " << n1 << " = " << j*n1 ;
			cout << endl;
		}
	}
	return 0;
}

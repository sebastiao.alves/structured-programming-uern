#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	const double JUROS_12=0.022, JUROS_13_24=0.02, JUROS_25_48=0.018, ENTRADA=0.2, SEGURO=0.04, TAC=500;
	double precoCarro, valorEntrada, parcela, valorFinanciado, total;
	cout << fixed << setprecision(2);
	cout << "Digite o valor do carro a ser financiado: ";
	cin >> precoCarro;
	do{
		cout << "Digite o valor da entrada (pelo menos " << ENTRADA*precoCarro << "): ";
		cin >> valorEntrada;
	}while(valorEntrada < ENTRADA*precoCarro);
	
	valorFinanciado=(precoCarro-valorEntrada+TAC)*(1+SEGURO);
	cout << valorFinanciado << endl;
	for(int prestacoes=12;prestacoes<=48;prestacoes++){
		double taxaJuros;
		if(prestacoes==12)
			taxaJuros=JUROS_12;
		else if(prestacoes>12 && prestacoes<=24)
			taxaJuros=JUROS_13_24;
		else
			taxaJuros=JUROS_25_48;
		total=valorFinanciado*(1+prestacoes*taxaJuros);
		parcela=total/prestacoes;
		cout << "O valor da parcela em " << prestacoes << "vezes é R$ " << parcela << ". Valor total pago: R$ "
		 << total+valorEntrada << endl;
	}
	
	return 0;
}

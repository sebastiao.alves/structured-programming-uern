#include <iostream>
using namespace std;
int main(){
	int numero, divisor=2;
	bool primeiroDivisor=true;
	cout << "Digite um número para exibi-lo como produto dos seus fatores primos: ";
	cin >> numero;
	cout << numero << " = ";
	do{
		if(numero%divisor==0){
			numero /= divisor; // numero = numero / divisor 
			if(primeiroDivisor){
				cout << divisor;
				primeiroDivisor=false;
			}else
				cout << " * " << divisor;
		}else
			++divisor;
	}while(numero>1);
	cout << endl;
	return 0;
}

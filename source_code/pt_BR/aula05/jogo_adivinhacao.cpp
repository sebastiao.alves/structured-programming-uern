#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(){
	
	const int TEMPO_LIMITE=60;
	
	int numero, usuario, tentativas=0, estrelas;
	
	// Sortear o número aleatório
	
	srand(time(NULL));
	numero=1+rand()%1024;
	
	// Começar a tentar adivinhar
	
	time_t horaAntes = time(NULL); // pega o tempo de inicio
	
	do{
		cout << "Digite um número entre 1 e 1024: ";
		cin >> usuario;
		tentativas++;
		
		if(numero<usuario)
			cout <<  " O número é menor! " << endl;
		else if(numero>usuario)
			cout <<  " O número é maior! " << endl;
			
	}while(numero!=usuario);
	
	time_t horaDepois = time(NULL);
	int tempoDecorrido = horaDepois - horaAntes;
	cout << "Você digitou em " << tempoDecorrido << " segundos." << endl;
	
	cout << " Você acertou em " << tentativas  << " chances!" << endl;
	
	if(tentativas <=5 && tempoDecorrido < TEMPO_LIMITE)
		estrelas=5;
	else if(tentativas <=8 && tempoDecorrido < TEMPO_LIMITE)
		estrelas=4;
	else if(tentativas <=10 && tempoDecorrido < TEMPO_LIMITE)
		estrelas=3;
	else if(tentativas <=8 && tempoDecorrido > TEMPO_LIMITE)
		estrelas=2;
	else if(tentativas <=10 && tempoDecorrido > TEMPO_LIMITE)
		estrelas=1;
	else
		estrelas=0;
		
	cout << " Sua pontuação foi de " << estrelas << " estrelas" << endl;
	return 0;
}

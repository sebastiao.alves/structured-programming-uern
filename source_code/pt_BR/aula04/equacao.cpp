#include <iostream>
#include <cmath>

using namespace std;

int main(){
	double a, b, c, delta, x1, x2;
	cout << "Digite os coeficientes de uma equação do 2° grau a, b e c, nesta ordem: ";
	cin >> a >> b >> c;
	
	delta = b*b - 4*a*c;
	
	if( delta < 0)
		cout << "Não existem raízes reais para esta equação." << endl;
	else if (delta == 0){
		x1 = -b / (2*a);
		cout << "A equação tem uma raiz real: " << x1 << endl;
	}else{
		x1 = (-b + sqrt(delta)) / (2*a);
		x2 = (-b - sqrt(delta)) / (2*a);
		cout << "A equação tem duas raízes reais: " << x1 << " e " << x2 << endl;
	}
	return 0;
}

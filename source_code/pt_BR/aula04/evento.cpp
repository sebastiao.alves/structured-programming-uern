#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	const double TAXA_ALUNO=30.0, TAXA_PROFESSOR=70.0, TAXA_PROFISSIONAL=100.0, TAXA_MINICURSO=20.0;
	const double TAXA_DEBITO=0.0199, TAXA_CREDITO=0.0498;
	const int OP_ALUNO=1, OP_PROFESSOR=2, OP_PROFISSIONAL=3;
	const int PAG_BOLETO=1, PAG_DEBITO=2, PAG_CREDITO=3;
	char fazMinicurso;
	int tipoInscricao, formaPagamento;
	double valorTotal;
	cout << "Digite a opção para inscrição no evento: \n1 - Aluno\n2 - Professor \n3 - Profissional\n";
	cin >> tipoInscricao;
	
	switch(tipoInscricao){
		case OP_ALUNO:
			valorTotal = TAXA_ALUNO;
			break;
		case OP_PROFESSOR:
			valorTotal = TAXA_PROFESSOR;
			break;
		case OP_PROFISSIONAL:
			valorTotal = TAXA_PROFISSIONAL;
			break;
		default:
			cout << "Opção inválida! Tente novamente." << endl;
                        return -1;
	}
	
	cout << "O participante fará minicurso? Digite s ou n: ";
	cin >> fazMinicurso;
	switch(fazMinicurso){
		case 'S':
		case 's':
			valorTotal += TAXA_MINICURSO;
			break;
		case 'n':
		case 'N':
			break;
		default:
			cout << "Opção inválida! Tente novamente." << endl;
                        return -1;
	}
	
	cout << "Digite a forma de pagamento: \n1 - Boleto\n2 - Cartão de débito\n3 - Cartão de crédito\n";
	cin >> formaPagamento;
	
	switch(formaPagamento){
		case PAG_BOLETO:
			break;
		case PAG_DEBITO:
			valorTotal = valorTotal + TAXA_DEBITO*valorTotal;
			break;
		case PAG_CREDITO:
			valorTotal = valorTotal + TAXA_CREDITO*valorTotal;
			break;
		default:
			cout << "Opção inválida!" << endl;
                        return -1;
	}
	
	cout << fixed << setprecision(2);
	cout << "O valor total a ser pago é : " << valorTotal << endl;
	
	return 0;
}

#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	const double FAIXA1=1693.72, FAIXA2=2822.90, TETO=5645.80;
	const double TAXA_FAIXA1=0.08, TAXA_FAIXA2=0.09, TAXA_TETO=0.11;
	double salarioBruto, salarioLiquido, descontoINSS;
	cout << "Digite o valor do salário bruto do trabalhador para saber \
seu salário líquido: ";
	cin >> salarioBruto;
	
	if ( salarioBruto <= FAIXA1 )
		descontoINSS = salarioBruto * TAXA_FAIXA1;
	else
		if ( salarioBruto <= FAIXA2 )
			descontoINSS = salarioBruto * TAXA_FAIXA2;
		else 
			if ( salarioBruto <= TETO )
				descontoINSS = salarioBruto * TAXA_TETO;
			else
				descontoINSS = TETO * TAXA_TETO;
	
	salarioLiquido = salarioBruto - descontoINSS;
	
	cout << fixed << setprecision(2);
	
	cout << "O desconto do INSS foi de : " << descontoINSS << endl;
	cout << "O salário líquido a receber é de : " << salarioLiquido	 << endl;
	
	return 0;
}

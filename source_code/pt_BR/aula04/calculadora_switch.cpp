#include <iostream>
#include <cmath>

using namespace std;

int main(){
	double operando1, operando2, resultado;
	char operacao;
	cout << "Digite uma expressão do tipo operador operando operador: " << endl;
	cin >> operando1 >> operacao >> operando2;
	
	switch(operacao){
		case '+':
			resultado = operando1 + operando2;
			break;
		case '-':
			resultado = operando1 - operando2;
			break;
		case '*':
			resultado = operando1 * operando2;
			break;
		case '/':
			resultado = operando1 / operando2;
			break;
		case '^':
			resultado = pow(operando1, operando2);
			break;
		default:
			cout << "Operador inválido!" << endl;
			return -1;
	}
	cout << "O resultado da operação é: " << resultado << endl;
	return 0;
}

#include <iostream>
#include <algorithm>
#include <ctime>

using namespace std;

int main(){
    string palavra, shuffle1, shuffle2, lido1, lido2;
    cout << "Digite uma palavra: " << endl;
    cin >> palavra;
	srand(time(NULL));
    shuffle1=palavra;
    shuffle2=palavra;

    random_shuffle(shuffle1.begin(), shuffle1.end());
    random_shuffle(shuffle2.begin(), shuffle2.end());

    cout << shuffle1 << " " << shuffle2 << endl;
    
    cout << "Digite-a o mais rápido que puder" << endl;
	
    time_t timestamp=time(NULL);
    cin >> lido1 >> lido2;
    int passou = time(NULL) - timestamp;
    
    cout << "Você gastou " << passou << " segundos para dar a resposta" << endl;
    
    int estrelas;
    
	if(lido1==shuffle1 && lido2==shuffle2 && passou<=10)
		estrelas=5;
	else if((lido1==shuffle1 || lido2==shuffle2) && passou<=10)
		estrelas=4;
	else if(lido1==shuffle1 && lido2==shuffle2 && passou>10)
		estrelas=3;
	else if((lido1==shuffle1 || lido2==shuffle2) && passou>10)
		estrelas=2;
	else if(lido1!=shuffle1 && lido2!=shuffle2 && passou<=10)
		estrelas=1;
	else
		estrelas=0;
		
	cout << "Sua pontuação foi de : " << estrelas << " estrelas" << endl;
		
    return 0;
}

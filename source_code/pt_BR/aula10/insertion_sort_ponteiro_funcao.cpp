#include <iostream>
#include <cstring>

using namespace std;

inline bool comparaSimples(const string &a, const string &b){
	return a>b;
}

inline bool comparaIgoraMaiusculas(const string &a, const string &b){
	return strcasecmp(a.c_str(), b.c_str())>0;
}

void ordenaVetor(string vetor[], size_t tamanho, bool (*compara)(const string& a, const string& b)=comparaSimples){
	for(size_t i=0;i<tamanho-1;i++)
		for(size_t j=i+1;j<tamanho;j++)
			if(compara(vetor[i],vetor[j])){
				string tmp=vetor[i];
				vetor[i]=vetor[j];
				vetor[j]=tmp;
			}
}

int main(){
	string times[6]={"corinthians", "Palmeiras", "flamengo", "Vasco", "são paulo", "botafogo"};
	ordenaVetor(times, 6);
	for(int i=0;i<6;i++)
		cout << times[i] << " " ;
	cout << endl;
	ordenaVetor(times, 6, comparaIgoraMaiusculas);
	for(int i=0;i<6;i++)
		cout << times[i] << " " ;
	cout << endl;
	return 0;
}

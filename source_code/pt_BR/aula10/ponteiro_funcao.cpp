#include <iostream>

using namespace std;

int soma(int a, int b){
	return a+b;
}

int subtracao(int a, int b){
	return a-b;
}

int main(){
	int (* operacao) (int, int);
	operacao=soma;
	cout << operacao(20,10) << endl;
	operacao=subtracao;
	cout << operacao(20,10) << endl;
	return 0;
}

#include <iostream>

using namespace std;

void exibeSituacao ( double media, int faltas, double media_aprovacao=7.0, double media_quarta=4.0, int max_faltas=15){
	if(faltas>max_faltas)
		cout << "Reprovado por falta." << endl;
	else if(media < media_quarta)
		cout << "Reprovado por media." << endl;
	else if(media <media_aprovacao)
		cout << "Quarta prova." << endl;
	else
		cout << "Aprovado." << endl;
}

int main(){
	double nota = 7.0;
	int faltas = 12;
	exibeSituacao(nota, faltas);
	exibeSituacao(nota, faltas, 6.0);
	exibeSituacao(nota, faltas, 7.0, 5.0);
	exibeSituacao(nota, faltas, 6.0, 3.0, 22);
	return 0;
}

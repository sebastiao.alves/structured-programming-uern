#include <iostream>
#include <cstdarg>

using namespace std;

double mediaPonderada (int N, ... ) {
	double soma=0.0;
	int somaPeso=0;
	va_list parametros;
	va_start(parametros, N);
	for(int i=0;i<N;i++){
		double nota = va_arg(parametros, double);
		int peso = va_arg(parametros, int);
		soma+=nota*peso;
		somaPeso+=peso;
	}
	va_end(parametros);
	return soma/somaPeso;
}

int main(){
	cout << mediaPonderada (2, 10.0, 2, 3.0, 5) << endl;
	cout << mediaPonderada (5, 10.0, 2, 3.0, 5, 4.5, 6, 9.0, 5, 10.0, 8) << endl;
	return 0;
}

#include <iostream>

using namespace std;

struct Aluno{
    string nome;
    int faltas;
    double notas[4];
};


struct NohAluno{
    Aluno aluno;
    NohAluno *ant, *prox;
};

struct ListaAlunos{
    NohAluno *prim, *ult;
    size_t tamanho;
};

ListaAlunos criaLista(){
    ListaAlunos l;
    l.prim = NULL;
    l.ult = NULL;
    l.tamanho = 0;
    return l;
}

void exibeAluno (Aluno a){
    cout << "Nome: " << a.nome << endl;
    cout << "Faltas: " << a.faltas << endl;
    for(size_t i=0;i<4;i++)
        cout << "Nota #" << i+1 << ": " << a.notas[i] << endl;
}

void exibeLista(ListaAlunos l){
    cout << "Conteudo da lista: " << endl;
    if(l.tamanho==0){
        cout << "Lista vazia!" << endl;
        return;
    }
    NohAluno *tmp=l.prim;
    for(size_t i=0;i<l.tamanho;i++){
        exibeAluno(tmp->aluno);
        tmp = tmp->prox;
    }
}

void insereLista(ListaAlunos &l, Aluno a, size_t pos){
    if(pos<0 || pos>l.tamanho){
        cout << "ERRO: inserir em posição inválida!!" << endl;
        return;
    }
    NohAluno *novo = new NohAluno;
    novo->aluno=a;
    if(l.tamanho==0){
        novo->ant=NULL;
        novo->prox=NULL;
        l.prim=novo;
        l.ult=novo;
    }else if(pos==0){
        novo->ant=NULL;
        novo->prox=l.prim;
        l.prim->ant=novo;
        l.prim=novo;
    }else if(pos==l.tamanho){
        novo->prox=NULL;
        novo->ant=l.ult;
        l.ult->prox=novo;
        l.ult=novo;
    }else{
        NohAluno *tmp=l.prim;
        for(int i=0;i<pos-1;i++)
            tmp=tmp->prox;
        novo->ant=tmp;
        novo->prox=tmp->prox;
        tmp->prox->ant=novo;
        tmp->prox=novo;
    }
    l.tamanho++;
}

int main(){
    ListaAlunos lista1 = criaLista();
    Aluno a={"Sebastião", 0, {10, 10, 10, -1}};
    Aluno a2 = {"Kergionaldo", 10, {3,4,5,6}};
    Aluno a3 = {"João", 10, {3,4,5,6}};
    exibeLista(lista1);

//  Aluno x = leAluno();
//  size_t pos = 0;
    insereLista(lista1, a, 0);
    insereLista(lista1, a2, 0);
    insereLista(lista1, a2, 2);
    insereLista(lista1, a3, 1);
    insereLista(lista1, a3, 3);

    exibeLista(lista1);
    return 0;
}

#include <iostream>
#include <algorithm>

using namespace std;

bool ordemCrescente(double a, double b){
	return a<b;
}

bool ordemDecrescente(double a, double b){
	return a>b;
}

int main(){
	double notas [7] = {2.1, 3.4, 0.5, 8.4, 9.2, 9.1, 1.7};
	sort (notas, notas+7, ordemCrescente);
	for (int i=0;i<7;i++)
		cout << notas[i] << " ";
	cout << endl;
	sort (notas, notas+7, ordemDecrescente);
	for (int i=0;i<7;i++)
		cout << notas[i] << " ";
	cout << endl;
	return 0;
}

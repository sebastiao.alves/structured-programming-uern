#include <iostream>
#include <cmath>
#include <iterator>

using namespace std;

void dobro(int &num){
    num *= 2;
}

void troca1 (int a, int b){
    int tmp=a;
    a=b;
    b=tmp;
}

void troca2 (int *a, int *b){
    int tmp=*a;
    *a=*b;
    *b=tmp;
}

void troca3 (int &a, int &b){
    int tmp=a;
    a=b;
    b=tmp;
}

void quadrado (double vetor[]){
    for(int i=0;i<size(vetor);i++)
        vetor[i] *= vetor[i];
}

int main(){
    //int x=10, y=20;
    //dobro(x);
    //dobro(y);
    //cout << x << "  " <<  y << endl;

    //troca1(x, y);
    //troca2(&x, &y);
    //troca3(x,y);
    //cout << x << "  " <<  y << endl;
    //
    double v [] = {1.3, 4.7, 9.1, -3.6};

    for(int i=0;i<sizeof(v)/sizeof(double);i++)
        cout << v[i] << endl;

    cout << endl;
    quadrado(v);

    for(int i=0;i<sizeof(v)/sizeof(double);i++)
        cout << v[i] << endl;

    return 0;
}

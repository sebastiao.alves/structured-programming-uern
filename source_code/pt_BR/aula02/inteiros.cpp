#include <iostream>

using namespace std;

int main()
{
    short s1=1000, s2=10000;
    int i1=s1+s2, i2=s1*s2;
    long long ll1=i1+i2, ll2=i1*i2;
   
    cout << "Shorts: " << s1 << " " << s2 << endl;
    cout << "Ints: " << i1 << " " << i2 << endl;
    cout << "Long long: " << ll1 << " " << ll2 << endl;
  
    unsigned char sc1 = 255;
    sc1++;
    sc1-=256;
    cout << sc1 << endl;
    return 0;
}

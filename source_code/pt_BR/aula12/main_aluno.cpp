#include <iostream>
#include <algorithm>
#include "aluno.h"

using namespace std;

int main(){
	Aluno turmaA[2];
	turmaA[0]={"joao", 5, {1.0, 5.5, 7.0}};
	turmaA[1]={"maria", 10, {1.0, 5.5, 7.0}};
	if(mediaAluno(turmaA[0])>=7.0)
		turmaA[0].status = APROVADO;
	if(turmaA[0]>turmaA[1])
		cout << "João tem média maior"<< endl;
	else
		cout << "João não tem média maior" << endl;
	if(turmaA[0]!=turmaA[1])
		cout << "São alunos diferentes" << endl;
	sort(turmaA, turmaA+2, comparaAluno);
	cout << turmaA[0].nome << " " << turmaA[1].nome << endl;
	return 0;
}

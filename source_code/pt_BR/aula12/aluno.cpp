#include <iostream>
#include "aluno.h"

inline double mediaAluno(const Aluno &a){
	return (4*a.notas[0]+5*a.notas[1]+6*a.notas[2])/15;
}

bool operator > (const Aluno &a, const Aluno &b){
	double mediaA=mediaAluno(a), mediaB=mediaAluno(b);
	if(mediaA>mediaB || (mediaA==mediaB && a.nome<b.nome))
		return false;
	return true;	
}

bool operator < (const Aluno &a, const Aluno &b){
	double mediaA=mediaAluno(a), mediaB=mediaAluno(b);
	if(mediaA<mediaB || (mediaA==mediaB && a.nome>b.nome))
		return false;
	return true;	
}

bool operator != (const Aluno &a, const Aluno &b){
	return a.nome!=b.nome;
}

bool comparaAluno(const Aluno &a, const Aluno &b){
	return a>b;
}

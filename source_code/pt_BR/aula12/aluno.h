
enum SituacaoAluno { MATRICULADO, TRANCADO, APROVADO, REPROVADO_FALTA,
	REPROVADO_MEDIA, QUARTA_PROVA};

struct Aluno{
	std::string nome;
	int faltas;
	double notas[3];
	SituacaoAluno status;
};


inline double mediaAluno(const Aluno &a);

bool operator > (const Aluno &a, const Aluno &b);

bool operator < (const Aluno &a, const Aluno &b);

bool operator != (const Aluno &a, const Aluno &b);

bool comparaAluno(const Aluno &a, const Aluno &b);

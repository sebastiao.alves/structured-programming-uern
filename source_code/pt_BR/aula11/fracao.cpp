#include <iostream>

using namespace std;

struct Fracao{
	int num, den;
};

Fracao operator * (const Fracao &a, const Fracao &b){
		Fracao resultado;
		resultado.num = a.num * b.num;
		resultado.den = a.den * b.den;
		return resultado;
}

Fracao operator * (int a, const Fracao &b){
	Fracao resultado;
	resultado.num = a*b.num;
	resultado.den = b.den;
	return resultado;
}

int main(){
	Fracao f1,f2;
	f1.num=5;
	f1.den=3;
	f2={1,2};
	Fracao f3=f1*f2;
	cout << f3.num << " / " << f3.den << endl;
	Fracao f4 = 3*f1;
	cout << f4.num << " / " << f4.den << endl;
	return 0;
}

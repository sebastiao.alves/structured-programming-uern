#include <iostream>
#include <algorithm>

using namespace std;

template <class Tipo>
struct No{
	Tipo dado;
	No *ant, *prox;
};

template <class Tipo>
struct Lista{
	No<Tipo> *inicio, *fim;
	size_t tamanho;
};

template <class Tipo>
void insereInicio(Lista<Tipo> &lista, Tipo dado){
	No<Tipo> * novo = new No<Tipo>;
	novo->ant=NULL;
	novo->prox=lista.inicio;
	novo->dado=dado;
	if(lista.inicio!=NULL)
		lista.inicio->ant=novo;
	else
		lista.fim=novo;
	lista.inicio=novo;
	lista.tamanho++;
}

template <class Tipo>
void exibeLista(Lista<Tipo> &lista){
	No<Tipo> *tmp=lista.inicio;
	while(tmp!=NULL){
		cout << tmp->dado << endl;
		tmp=tmp->prox;
	}
}

int main(){
	Lista <string> l1={NULL, NULL,0};
	Lista <int> l2{NULL, NULL,0};
	string s1="sebastiao", s2="emidio", s3="alves", s4="filho";
	insereInicio(l1, s1);
	insereInicio(l1, s2);
	insereInicio(l1, s3);
	insereInicio(l1, s4);
	exibeLista(l1);
	insereInicio(l2, 10);
	insereInicio(l2, 50);
	insereInicio(l2, 30);
	insereInicio(l2, 40);
	exibeLista(l2);
	return 0;
}

#include <iostream>
#include <algorithm>

using namespace std;

struct Aluno{
	string nome;
	int faltas;
	double notas[3];
};

inline Aluno * alocaTurma (size_t tam){
	return new Aluno[tam];
}

inline void desalocaTurma(Aluno *turma){
	delete []turma;
}

void leDadosAluno (Aluno *a){
	cout << "Digite o nome do aluno:";
	cin >> (*a).nome;
	cout << "Digite o número de faltas do aluno:";
	cin >> a->faltas;
	cout << "Digite as três notas do aluno separadas por espaço: ";
	cin >> a->notas[0] >> a->notas[1] >> a-> notas[2];
}

void leDadosAluno (Aluno &a){
	cout << "Digite o nome do aluno:";
	cin >> a.nome;
	cout << "Digite o número de faltas do aluno:";
	cin >> a.faltas;
	cout << "Digite as três notas do aluno separadas por espaço: ";
	cin >> a.notas[0] >> a.notas[1] >> a.notas[2];
}

inline double mediaAluno(const Aluno &a){
	return (4*a.notas[0]+5*a.notas[1]+6*a.notas[2])/15;
}

bool operator > (const Aluno &a, const Aluno &b){
	double mediaA=mediaAluno(a), mediaB=mediaAluno(b);
	if(mediaA>mediaB || (mediaA==mediaB && a.nome<b.nome))
		return false;
	return true;	
}
bool comparaAluno(const Aluno &a, const Aluno &b){
	return a>b;
}

int main(){
	Aluno *turmaA;
	turmaA=alocaTurma(2);
	turmaA[0]={"joao", 5, {1.0, 5.5, 7.0}};
	turmaA[1]={"maria", 10, {1.0, 5.5, 7.0}};
	//leDadosAluno(&turmaA[0]);
	//leDadosAluno(turmaA[1]);
	sort(turmaA, turmaA+2, comparaAluno);
	cout << turmaA[0].nome << " " << turmaA[1].nome << endl;
	desalocaTurma(turmaA);
	return 0;
}

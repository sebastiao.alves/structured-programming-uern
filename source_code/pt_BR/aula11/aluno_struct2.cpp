#include <iostream>
#include <algorithm>

using namespace std;

enum SituacaoAluno { MATRICULADO, TRANCADO, APROVADO, REPROVADO_FALTA,
	REPROVADO_MEDIA, QUARTA_PROVA};

struct Aluno{
	string nome;
	int faltas;
	double notas[3];
	SituacaoAluno status;
};


inline double mediaAluno(const Aluno &a){
	return (4*a.notas[0]+5*a.notas[1]+6*a.notas[2])/15;
}

bool operator > (const Aluno &a, const Aluno &b){
	double mediaA=mediaAluno(a), mediaB=mediaAluno(b);
	if(mediaA>mediaB || (mediaA==mediaB && a.nome<b.nome))
		return false;
	return true;	
}

bool operator < (const Aluno &a, const Aluno &b){
	double mediaA=mediaAluno(a), mediaB=mediaAluno(b);
	if(mediaA<mediaB || (mediaA==mediaB && a.nome>b.nome))
		return false;
	return true;	
}

bool operator != (const Aluno &a, const Aluno &b){
	return a.nome!=b.nome;
}

bool comparaAluno(const Aluno &a, const Aluno &b){
	return a>b;
}

int main(){
	Aluno turmaA[2];
	turmaA[0]={"joao", 5, {1.0, 5.5, 7.0}};
	turmaA[1]={"maria", 10, {1.0, 5.5, 7.0}};
	if(mediaAluno(turmaA[0])>=7.0)
		turmaA[0].status = APROVADO;
	if(turmaA[0]>turmaA[1])
		cout << "João tem média maior"<< endl;
	else
		cout << "João não tem média maior" << endl;
	if(turmaA[0]!=turmaA[1])
		cout << "São alunos diferentes" << endl;
	sort(turmaA, turmaA+2, comparaAluno);
	cout << turmaA[0].nome << " " << turmaA[1].nome << endl;
	return 0;
}

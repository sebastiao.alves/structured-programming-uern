#include <iostream>
#include <cmath>

using namespace std;

int main(){
	int n;
	cout << "Digite a dimensão dos vetores: ";
	cin >> n;
	double v1[n], v2[n], angulo;
	for (int i=0;i<n;i++){
		cout << "Digite o valor de v1[" << i << "]: ";
		cin >> v1[i];
	}
	for (int i=0;i<n;i++){
		cout << "Digite o valor de v2[" << i << "]: ";
		cin >> v2[i];
	}
	
	// Cálculo do produto escalar	
	double produtoEscalar=0.0;
	for (int i=0;i<n;i++)
		produtoEscalar+= v1[i]*v2[i];
		
	// Cálculo do módulo de v1
	double moduloV1=0.0;
	for (int i=0;i<n;i++)
		moduloV1 += pow(v1[i],2);
	moduloV1 = sqrt(moduloV1);
	
	// Cálculo do módulo de v2
	double moduloV2=0.0;
	for (int i=0;i<n;i++)
		moduloV2 += pow(v2[i],2);
	moduloV2 = sqrt(moduloV2);
	
	angulo = acos (produtoEscalar / (moduloV1*moduloV2))* 180.0/M_PI;
	
	cout << "O ângulo entre os vetores em graus é : " << angulo << endl;
	
	return 0;
}

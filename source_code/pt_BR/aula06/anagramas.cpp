#include <iostream>
#include <algorithm>

using namespace std;

int main(){
	const int TAMANHO=5;
	char vetor[TAMANHO];
	cout << "Digite uma string de " << TAMANHO-1 << " caracteres : ";
	cin >> vetor;
	for (int i=0;i<TAMANHO-1;i++)
		for (int j=i+1; j<TAMANHO-1;j++)
			if(vetor[j]<vetor[i]){
				char aux = vetor[i];
				vetor[i]=vetor[j];
				vetor[j]=aux;
			}
	
	cout << "Todos os anagramas possíveis: " << endl;
	do{
		cout << vetor << endl;
	}while(next_permutation(vetor, vetor+TAMANHO-1));
	return 0;
}

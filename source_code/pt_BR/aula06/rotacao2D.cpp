#include <iostream>
#include <cmath>

using namespace std;

int main(){
	int pontos;
	cout << "Digite o número de pontos de uma figura 2D:";
	cin >> pontos;
	
	double figura [pontos][2];
	
	for (int i=0;i<pontos;i++){
		cout << "Digite as coordenadas x e y do " << (i+1) << "º ponto:" ;
		cin >> figura[i][0] >> figura[i][1];
	}
	
	
	cout << "Figura não rotacionada:" << endl;
	for(int i=0;i<pontos;i++)
		cout << figura[i][0] << " " << figura[i][1] << endl;
		
	double angulo;
	cin >> angulo;
	cout << "Digite o ângulo de rotação da figura: ";
	angulo=angulo*M_PI/180;
	
	double R[2][2]={{cos(angulo), sin(angulo)},
		 {-sin(angulo), cos(angulo)}};
		 
		 
	double figuraRotacionada [pontos][2];
	for(int i=0;i<pontos;i++)
		for(int j=0;j<2;j++){
			figuraRotacionada[i][j]=0.0;
			for(int k=0;k<2;k++){
				figuraRotacionada[i][j]+=figura[i][k]*R[k][j];
			}
		}
	
	cout << "Figura rotacionada:" << endl;
	for(int i=0;i<pontos;i++)
		cout << figuraRotacionada[i][0] << " " << figuraRotacionada[i][1] << endl;
	
	return 0;
}

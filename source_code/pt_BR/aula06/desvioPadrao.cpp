#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(){
	int num_alunos;
	double media=0.0, desvioPadrao=0.0;
	cout << "Digite o número de alunos de uma sala: ";
	cin >> num_alunos;
	double notas[num_alunos];
	for (int i=0;i<num_alunos;i++){
		cout << "Digite a nota do " << (i+1) << "° aluno: ";
		cin >> notas[i];
		media += notas[i];
	}
	media/=num_alunos;
	// Cálculo do desvio padrão
	for (int i=0;i<num_alunos;i++)
		desvioPadrao += pow(notas[i]-media,2);
	desvioPadrao = sqrt(desvioPadrao);
	cout << fixed << setprecision(1);
	cout << "A média das notas dos alunos foi de " << media 
	 << " com desvio padrão de " << desvioPadrao << endl;
	return 0;
}

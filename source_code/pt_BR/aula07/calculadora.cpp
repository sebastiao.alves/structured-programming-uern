#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char **argv){
	double operando1, operando2;
	char operacao;
	//cout << "Digite uma expressão no formato numero operação numero: " << endl;
	//cin >> operando1 >> operacao >> operando2;
	if(argc!=4){
		cout << "Erro! Use ./a.out num operador num\nExemplo: ./a.out 10 + 30" << endl;
		return -1;
	}
	operando1=atof(argv[1]);
	operando2=atof(argv[3]);
	operacao = *argv[2];
	
	double resultado;
	switch(operacao){
		case '+':
			resultado = operando1 + operando2;
			break;
		case '-':
			resultado = operando1 - operando2;
			break;
		case '*':
			resultado = operando1 * operando2;
			break;
		case '/':
			resultado = operando1 / operando2;
			break;
		case '^':
			resultado = pow (operando1, operando2);
			break;
		default:
			cout << "Operação não disponível" << endl;
			return -1;
	}
	cout << "O resultado da operação é : " << resultado << endl;
	return 0;
}

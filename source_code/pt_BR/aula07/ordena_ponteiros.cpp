#include <iostream>

using namespace std;

int main(){
	int vetor [] = {2,4,6,1,9,3,5};
	int *ptrs[7];
	for(int i=0;i<7;i++)
		ptrs[i] = vetor+i;
		
	for(int i=0;i<6;i++)
		for(int j=i+1;j<7;j++)
			if(*ptrs[i]>*ptrs[j]){
				int *aux = ptrs[i];
				ptrs[i]=ptrs[j];
				ptrs[j]=aux;
			}
	for (int tmp:vetor)
		cout << tmp << " ";
	cout << endl;
	for (int *tmp:ptrs)
		cout << *tmp << " ";
	cout << endl;
	
	for (int *tmp:ptrs)
		cout << tmp << " ";
	cout << endl;
	return 0;
}

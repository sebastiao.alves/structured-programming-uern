#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	const int TAM_MAX=20;
	char str [TAM_MAX];
	cout << "Digite uma string de, no máximo, " << TAM_MAX-1 
		<< " caracteres para saber se é um palíndrome: " << endl;
	cin >> setw(TAM_MAX) >> str;
	int cont=0;
	char *ptr = str;
	while(*ptr!='\0'){
		ptr++;
		cont++;
	}
	cout << "Você digitou uma string de " << cont << " caracteres." << endl;
	char *inicio=str, *fim=str+cont-1;
	bool ehPalindrome=true;
	while(inicio<fim){
		if(*inicio != *fim){
			ehPalindrome=false;
			break;
		}
		inicio++;
		fim--;
	}
	
	if(ehPalindrome)
		cout << "A string é um palíndrome." << endl;
	else
		cout << "A string não é um palíndrome." << endl;
	return 0;
}




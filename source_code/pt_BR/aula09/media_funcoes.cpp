/* Faça um programa que leia um vetor de reais e mostre a média aritmética, 
 * o desvio padrão e o vetor ordenado */

#include <iostream>
#include <cmath>

using namespace std;

double media (double *vetor, size_t tam){
	double m = 0.0;
	for(size_t i=0;i<tam;i++)
		m+=vetor[i];
	return m/tam;
}

double desvioPadrao(double *vetor, size_t tam){
	double desvio=0.0, m = media(vetor, tam);
	for(size_t i=0;i<tam;i++)
		desvio+= pow(vetor[i]-m, 2.0);
	return sqrt(desvio/tam);
}

void exibeVetor(double *vetor, size_t tam){
		for(size_t i=0;i<tam;i++)
			cout << vetor[i] << " ";
		cout << endl;
}

void ordenaVetor(double *vetor, size_t tam){
	for(size_t i=0;i<tam-1;i++)
		for(size_t j=i+1;j<tam;j++)
			if(vetor[i]>vetor[j]){
				double tmp=vetor[i];
				vetor[i]=vetor[j];
				vetor[j]=tmp;
			}
}

int main(){
	double *v = new double[3];
	v[0]=1.80;
	v[1]=1.55;
	v[2]=1.70;
	cout << "Media: " << media(v, 3) << endl;
	cout << "Desvio: " << desvioPadrao(v, 3) << endl;
	exibeVetor(v, 3);
	ordenaVetor(v,3);
	exibeVetor(v, 3);
	delete []v;
    return 0;
}

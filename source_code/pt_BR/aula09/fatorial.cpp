#include <iostream>

using namespace std;

int fatorial (int n){
    int fat=1;
    while(n>1){
        fat*=n;
        n--;
    }
    cout << "N: " << n << endl;
    return fat;
}

int main(){
    int x=5, y=2, f=fatorial(x), g=fatorial(y);
    cout << " X: " << x << "  f: " << f << endl;
    cout << " Y: " << y << "  g: " << g << endl;
    return 0;
}

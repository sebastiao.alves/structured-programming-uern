/* Faça um programa que leia um número n, crie um vetor de booleanos 
 * com o conteúdo verdadeiro se o número for primo e falso se não for. 
 * Exiba então os números primos de 2 até N. */

#include <iostream>

using namespace std;

bool *alocaVetor(size_t tam){
	bool *vetor = new bool[tam];
	for (size_t i=0;i<tam;i++)
		vetor[i]=true;
	return vetor;
}

void crivoDeErastotenes (bool *vetor, size_t tam){
	vetor[0]=vetor[1]=false;
	for(size_t i=2; i<tam;i++){
		if(vetor[i]){
			size_t mult=2;
			while(i*mult<tam){
				vetor[i*mult]=false;
				mult++;
			}
		}
	}
}

void exibePrimos(bool *vetor, size_t tam){
		for(size_t i=2;i<tam;i++)
			if(vetor[i])
				cout << i << " ";
		cout << endl;
}

void desalocaVetor(bool *vetor){
	delete []vetor;
}

int main(){
	bool *primos = alocaVetor (1000);
	crivoDeErastotenes(primos, 1000);
	exibePrimos(primos, 1000);
	desalocaVetor(primos);
    return 0;
}

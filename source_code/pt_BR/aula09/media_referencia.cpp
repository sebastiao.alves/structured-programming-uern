/* Faça um programa que leia um vetor de reais e mostre a média aritmética, 
 * o desvio padrão e o vetor ordenado */

#include <iostream>
#include <cmath>

using namespace std;

void calcMediaDesvio (double vetor[], size_t tam, double &media, double &desvio){
	media=desvio=0.0;
	for(size_t i=0;i<tam;i++)
		media+=vetor[i];
	media /=tam;
	for(size_t i=0;i<tam;i++)
		desvio+= pow(vetor[i]-media, 2.0);
	desvio=sqrt(desvio/tam);
}

void exibeVetor(double vetor[ ], size_t tam){
		for(size_t i=0;i<tam;i++)
			cout << vetor[i] << " ";
		cout << endl;
}

void ordenaVetor(double vetor[ ] , size_t tam){
	for(size_t i=0;i<tam-1;i++)
		for(size_t j=i+1;j<tam;j++)
			if(vetor[i]>vetor[j]){
				double tmp=vetor[i];
				vetor[i]=vetor[j];
				vetor[j]=tmp;
			}
}

int main(){
	double notas[] = {3.5, 4.5, 8.2, 2.9}, media, desvioPadrao;
	ordenaVetor(notas, 4);
	exibeVetor(notas, 4);
	calcMediaDesvio(notas, 4, media, desvioPadrao);
	cout << "Media " << media << "\nDesvio padrão " << desvioPadrao << endl;
    return 0;
}

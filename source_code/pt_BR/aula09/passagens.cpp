#include <iostream>

/* Uma companhia aérea tem um programa para alocar
passageiros em um avião, cujos assentos são numerados de 1 a
200. A princípio todos os assentos estão disponíveis e o
operador do software pode: Reservar um assento disponível,
onde deve inserir o nome do passageiro; Cancelar uma reserva
de assento, tornando o lugar disponível novamente; Listar os
nomes todos os passageiros do voo; Informar quantos lugares
ainda estão disponíveis. Faça um programa que ofereça um
menu com essas opções para o usuário até que ele escolha sair */

using namespace std;

bool * alocaAssentos (int numAssentos){
	return new bool [numAssentos];
}

string * alocaNomes (int numAssentos){
	return new string[numAssentos];
}

void desalocaDados(bool *assentos, string * nomes){
	delete [] assentos;
	delete [] nomes;
}

void inicializaAssentos (bool *assentos){
	// TODO: deixar todos os assentos disponíveis
}

bool ehOpcaoValida(int opcao){
	// TODO: recebe a opção escolhida pelo usuário e diz se ela é válida ou não
	return false;
}

int leOpcao(){
	// TODO: exibir as opções disponíveis para o usuário e ler a opção escolhida
	return 0;
}

void executaOpcaoEscolhida(int opcao, bool * assentos, string *nomes){
	// TODO: executa a opção desjada
	reservaAssento(int poltrona);
	// reservarAssento
		// cancelarReserva
			// estaDisponivel
			// alterarReserva
		// listarNomes
		// oterLugaresDisponiveis
}


int main(){
	const int NUM_ASSENTOS=200, RESERVA=1, CANCELAMENTO=2, LISTA=3, DISPONIVEIS=4, ENCERRA=5;
	bool *reservados;
	string *nomes;
	
	// Aloca os dados a serem manipulados
	reservados = alocaAssentos (NUM_ASSENTOS);
	nomes = alocaNomes (NUM_ASSENTOS);
	
	// Diz que todos os assentos estão disponíveis
	inicializaAssentos(reservados);

	int opcao;
	do{
		opcao = leOpcao();
		executarOpcaoEscolhida(opcao, reservados, nomes);
	}while (opcao!=ENCERRA);
	
	// Desaloca os dados alocados para os nomes e assentos
	desalocaDados(reservados, nomes);
	return 0;
}


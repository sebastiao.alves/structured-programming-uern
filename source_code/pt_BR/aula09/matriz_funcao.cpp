#include <iostream>

using namespace std;

void exibeMatriz(int **matriz, int linhas, int colunas){
	cout << "Matriz: " << endl;
	for(int i=0;i<linhas;i++){
		for(int j=0;j<colunas;j++)
			cout << matriz[i][j] << " ";
		cout << endl;
	}
}

int ** alocaMatriz (int linhas, int colunas){
	int **matriz;
	matriz = new int *[linhas];
	for(int i=0;i<linhas;i++)
		matriz[i]=new int[colunas];
	return matriz;
}

void leMatriz (int **matriz, int linhas, int colunas){
	cout << "Leitura da matriz " << endl;
	for(int i=0;i<linhas;i++)
		for(int j=0;j<colunas;j++){
			cout << "Digite o elemento [" << i << "][" << j << "]: ";
			cin >> matriz[i][j];
		}
}

void trocaLinhas(int **matriz, int linhas, int colunas, int origem, int destino){
	for(int i=0;i<colunas;i++){
		int tmp = matriz[origem][i];
		matriz[origem][i] = matriz[destino][i];
		matriz[destino][i] = tmp;
	}
}

void trocaColunas(int **matriz, int linhas, int colunas, int origem, int destino){
	for(int i=0;i<linhas;i++){
		int tmp = matriz[i][origem];
		matriz[i][origem] = matriz[i][destino];
		matriz[i][destino] = tmp;
	}
}

int leOpcao (string *opcoes, int numOpcoes){
	cout << "Escolha uma das opcoes: " << endl;
	for(int i=0;i<numOpcoes;i++){
		cout << (i+1) << " - " << opcoes[i] << endl;
	}
	cout << "Digite a opção desejada :";
	int op;
	cin >> op;
	return op;
}

int main(){
	int **mat;
	string *opcoes=new string[3];
	opcoes[0]="Trocar colunas";
	opcoes[1]="Trocar linhas";
	opcoes[2]="Sair";
	mat=alocaMatriz(3,3);
	leMatriz(mat, 3, 3);
	int opcao = leOpcao(opcoes, 3);
	if(opcao==1)
		trocaLinhas(mat, 3, 3, 0, 1);
	else if(opcao==2)
		trocaColunas(mat, 3, 3, 1, 2);
	exibeMatriz(mat, 3, 3);
	/*
	exibeMatriz(mat, 3, 3);
	trocaLinhas(mat, 3, 3, 0, 1);
	exibeMatriz(mat, 3, 3);
	trocaColunas(mat, 3, 3, 1, 2);
	exibeMatriz(mat, 3, 3);
	*/
	return 0;
}
